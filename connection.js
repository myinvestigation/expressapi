var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/demo', { useMongoClient: true });

var dbMongo = mongoose.connection;

dbMongo.on('error', console.error.bind(console, 'connection error:'));

dbMongo.once('open', function(){
    console.log('MongoDb connected');
});


module.exports = mongoose;