var express = require('express');
var router = express.Router();

var UsersController = require("../controllers/Users");


router.get('/:id?',function(req,res,next){
    if(req.params.id){
        UsersController.get(req, res);
    }else{
        UsersController.index(req, res);
    }
});

router.post('/',function(req,res,next){
    UsersController.create(req, res);
});

router.delete('/:id',function(req,res,next){
    UsersController.delete(req, res);
});

router.put('/:id',function(req,res,next){
    UsersController.update(req, res);
});

router.post('/login',function(req,res,next){
    UsersController.login(req, res);
});

router.post('/logout',function(req,res,next){
    UsersController.logout(req, res);
});

router.post('/info_login',function(req,res,next){
    // console.log("info_login")
    UsersController.login_info(req, res);
});

module.exports = router;
