var User = require("../models/User");

var UsersController = {
    index: function(req, res) {
        User.getAll(function(err, user) {
            if (!err)
                res.json(user);
            else
                res.json({success:false});
        });
        
    },

    get: function(req, res) {
        var id = req.params.id;
        User.findById(id, function(err, user){
            if(user)
                res.json(user);
            else
                res.json({success:false, message:"No found id:" + id});
        });
    },

    create: function(req, res) {
        var data = req.body;

        User.add(data, function(err, rs){
            console.log(err);
            if(err) 
                res.json({success:false});
            else
                res.json({success:true});
        });
    },

    update: function(req, res) {
        console.log("update");
        var data = req.body;
        var id = req.params.id;
        delete data["_id"];
        User.update(id, data, function(err) {
            console.log(data);
            if(err) 
                res.json({success:false, message: "Update error"});
            else
                res.json({success:true});
        });
    },

    delete: function(req, res) {
        var id = req.params.id;
        User.delete(id, function(err){
            if(err) 
                res.json({success:false});
            else
                res.json({success:true});
        });
    },

    login: function(req, res){
        var username = req.body["username"];
        var password = req.body["password"];
        console.log("Login: "+username + "/" + password);
        console.log(req.session);
        if (req.session.user) {
            res.json({success:false, message: "You logined"});
            return;
        }

        User.find({"username":username}, function(err, user){
            console.log("pass : " + user)
            if (user.length >0 && user[0].password == password){
                req.session.user = {"username": username};
                res.json({success:true});
                console.log(req.session);
            }
            else {
                res.json({success:false});
            }
        })
        
    },

    logout: function(req, res){
        // console.log("Login: "+username + "/" + password);
        console.log(req.session);
        if (!req.session.user) {
            res.json({success:false, message: "You are not logined"});
            return;
        }

        req.session.user = null;

        res.json({success:true});


    },

    login_info: function(req, res){
        // console.log("Login: "+username + "/" + password);
        console.log("login_info");
        console.log(req.session);
        if (!req.session.user) {
            res.json({success:false, message: "You are not logined"});
            return;
        }

        res.json({success:true, user:req.session.user});
    }

};

module.exports = UsersController;