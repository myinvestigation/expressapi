// require connection

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    username : String,
    password : String,
    first_name : String,
    last_name : String,
    dob: Date,
    email: String,
    mobile_number : String,
    remark: String
});

var User = mongoose.model('Users', UserSchema);


var UserModel={
    getAll:function(callback){
        var users = [];
        User.find({}, function (err, user) {
            callback(err, user);
        });
        return users;
    },
    findById:function(id, callback){
        User.findById(id, function (err, user) {
            callback(err, user);
        });
    },
    add:function(data, callback){
        var user = new User(data);
        console.log(user);
        user.save(function (err, rs) {
            // if (err) return handleError(err);
            callback(err, rs);
            // saved!
        });

    },
    delete:function(id, callback){
        User.remove({"_id":id}, function(err){
            callback(err);
        });
    },
    update:function(id, data, callback){
        User.update({"_id":id}, {"$set":data}, function(err){
            callback(err);
        });
        
    },
    find:function(where, callback){
        User.find(where, function (err, user) {
            callback(err, user);
        });
    },
};
module.exports=UserModel;